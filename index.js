import express from "express";
import bodyParser from "body-parser";
import cors from 'cors';
import soap from 'soap';
import multer from 'multer';
const app = express();
const PORT = 5050;
app.use(bodyParser.json({ limit: '30mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '30mb' }));
app.use(cors());
var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null,'/home/cannt/blog/server/cannt');
  },
  filename: function (req, file, callback) {
  console.log("🚀 ~ file: index.js:19 ~ file:", file)

    callback(null, file.originalname);
  }
});
var upload = multer({ storage : storage}).single('userPhoto');
 
app.post('/upload-avatar',function(req,res){
    console.log("🚀 ~ file: index.js:22 ~ app.post ~ req:", req)
    upload(req,res,function(err) {
        if(err) {
            console.log("🚀 ~ file: index.js:25 ~ upload ~ err:", err)
            return res.end("Error uploading file.");
        }
        res.end("File is uploaded");
    });
});
 
app.listen(PORT,function(){
    console.log("Working on port 3000");
});
// app.post('/send-sms', upload.single('image'), async (req, res) => {
//     console.log("🚀 ~ file: index.js:19 ~ app.post ~ req:", req)
//     // const imagePath = path.join(__dirname, '/public/images');
//     // console.log("🚀 ~ file: index.js:20 ~ app.post ~ imagePath:", imagePath)
//     console.log("🚀 ~ file: index.js:11 ~ app.post ~ req:", req.image)
//     // const { body } = req;
//     // const { content, phone } = body;
//     // const args = {
//     //     User: "smsbrand_xangdauna",
//     //     Password: "xd@258369",
//     //     CPCode: "XANGDAUNA",
//     //     RequestID: "1",
//     //     UserID: phone,
//     //     ReceiverID: phone,
//     //     ServiceID:"CtyXdauN.an",
//     //     CommandCode: "bulksms",
//     //     Content: content,
//     //     ContentType: "F"
//     // };

//     // soap.createClient('http://ams.tinnhanthuonghieu.vn:8009/bulkapi?wsdl', (error, client) => {
//     //     if (error) {
//     //         console.log("🚀 ~ file: index.js ~ line 24 ~ soap.createClient ~ error", error)
//     //     } else {
//     //         client.wsCpMt(args, (err, result) => {
//     //             console.log("🚀 ~ file: index.js ~ line 32 ~ client.wsCpMt ~ err", err)
//     //             console.log("🚀 ~ file: index.js ~ line 30 ~ client.wsCpMt ~ result", result)
//     //             console.log("🚀 ~ file: index.js ~ line 38 ~ client.wsCpMt ~ result.return.result", result.return.result)
//     //             if(result.return.result == 1) {
//     //                 res.send('Đã gửi thành công')
//     //             }
//     //         });
//     //     }
//     // });
//     res.send('Đã gửi thành công')
// });
// app.listen(PORT, () => {
//     console.log(`Server is running on port ${PORT}`)
// })